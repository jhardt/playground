//
//  MSOAppDelegate.h
//  MSOInspireLauncher
//
//  Created by Julius Hardt on 15/04/14.
//  Copyright (c) 2014 Julius Hardt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MSOAppDelegate : NSObject <NSApplicationDelegate> {
    int countdown;
    NSTimer* timer;
}

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSTextField *textField;
@property int countdown;
@property (retain) NSTimer *timer;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification;
- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication;
- (void)updateUserInterface;
- (IBAction)startInspire:(id)sender;
- (IBAction)quit:(id)sender;
- (void)tick:(NSTimer*)timer;

@end
