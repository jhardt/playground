//
//  MSOAppDelegate.m
//  MSOInspireLauncher
//
//  Created by Julius Hardt on 15/04/14.
//  Copyright (c) 2014 Julius Hardt. All rights reserved.
//

#import "MSOAppDelegate.h"

@implementation MSOAppDelegate

@synthesize countdown;
@synthesize timer;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    NSBeep();
    [[self window] center];
    [self setCountdown:10];
    [self updateUserInterface];
    [self setTimer:[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(tick:) userInfo:nil repeats:YES]];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication {
    return YES;
}

- (void)updateUserInterface
{
    if ([self countdown] == 1) {
        [[self textField] setStringValue:[NSString stringWithFormat:@"Das Programm wird in %d Sekunde automatisch gestartet, wenn Sie nichts tun.", [self countdown]]];
    }
    else {
        [[self textField] setStringValue:[NSString stringWithFormat:@"Das Programm wird in %d Sekunden automatisch gestartet, wenn Sie nichts tun.", [self countdown]]];
    }
}

- (IBAction)startInspire:(id)sender {
    [[self timer] invalidate];
    if ([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/MSOInspireControls.app"]) {
        [[NSWorkspace sharedWorkspace] launchApplication:@"/Applications/MSOInspireControls.app"];
    }
    if([[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/Promethean/Activsoftware Inspire/Inspire.app"]) {
        [[NSWorkspace sharedWorkspace] launchApplication:@"/Applications/Promethean/Activsoftware Inspire/Inspire.app"];
    }
    [self quit:nil];
}

- (IBAction)quit:(id)sender {
    [[self timer] invalidate];
    [[NSApplication sharedApplication] terminate:self];
}

- (void)tick:(NSTimer*)timer
{
    if ([self countdown] > 1) {
        [self setCountdown:[self countdown] - 1];
        [self updateUserInterface];
    }
    else {
        [self startInspire:nil];
    }
}

@end
