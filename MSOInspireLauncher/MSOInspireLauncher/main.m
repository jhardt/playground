//
//  main.m
//  MSOInspireLauncher
//
//  Created by Julius Hardt on 15/04/14.
//  Copyright (c) 2014 Julius Hardt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
