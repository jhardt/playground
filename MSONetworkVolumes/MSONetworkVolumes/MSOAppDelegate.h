//
//  MSOAppDelegate.h
//  MSONetworkVolumes
//
//  Created by Julius Hardt on 22/04/14.
//  Copyright (c) 2014 Julius Hardt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MSOAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSPanel *mountSheet;
@property (assign) IBOutlet NSTextField *mountUsernameField;
@property (assign) IBOutlet NSSecureTextField *mountPasswordField;
@property (assign) IBOutlet NSProgressIndicator *mountProcessIndicator;
@property (assign) IBOutlet NSPanel *unmountSheet;
@property (assign) IBOutlet NSPopUpButton *unmountSelect;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification;
- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication;
- (IBAction)showMountSheet:(id)sender;
- (IBAction)endMountSheet:(id)sender;
- (IBAction)mount:(id)sender;
- (IBAction)showUnmountSheet:(id)sender;
- (IBAction)endUnmountSheet:(id)sender;
- (IBAction)unmount:(id)sender;

@end
