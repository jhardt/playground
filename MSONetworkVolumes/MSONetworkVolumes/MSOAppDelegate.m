//
//  MSOAppDelegate.m
//  MSONetworkVolumes
//
//  Created by Julius Hardt on 22/04/14.
//  Copyright (c) 2014 Julius Hardt. All rights reserved.
//

#import "MSOAppDelegate.h"
#import <NetFS/NetFS.h>
#import <sys/mount.h>

@implementation MSOAppDelegate

@synthesize mountSheet;
@synthesize mountUsernameField;
@synthesize mountPasswordField;
@synthesize mountProcessIndicator;
@synthesize unmountSheet;
@synthesize unmountSelect;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    [[self window] center];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication {
    return YES;
}

- (IBAction)showMountSheet:(id)sender
{
    [[NSApplication sharedApplication] beginSheet:[self mountSheet] modalForWindow:[self window] modalDelegate:self didEndSelector:nil contextInfo:nil];
}

- (IBAction)endMountSheet:(id)sender
{
    [[NSApplication sharedApplication] endSheet:[self mountSheet]];
    [[self mountSheet] orderOut:sender];
}

- (IBAction)mount:(id)sender
{
    // NSLog(@"%@", [[self mountUsernameField] stringValue]);
    // NSLog(@"%@", [[self mountPasswordField] stringValue]);
    if ([[[self mountUsernameField] stringValue] length] == 0 || [[[self mountPasswordField] stringValue] length] == 0) {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Bitte füllen Sie die beiden Felder aus."];
        [alert runModal];
    }
    else {
        [[self mountProcessIndicator] setHidden:NO];
        [[self mountProcessIndicator] startAnimation:sender];
        
        NSString *username = [[self mountUsernameField] stringValue];
        NSString *password = [[self mountPasswordField] stringValue];
        NSURL *share = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", @"afp://intern", username]];
        NSURL *path = [NSURL fileURLWithPath:@"/Volumes" isDirectory:YES];
        CFArrayRef mountpoints = NULL;
        
        OSStatus result = NetFSMountURLSync((__bridge CFURLRef)(share), (__bridge CFURLRef)(path), (__bridge CFStringRef)(username), (__bridge CFStringRef)(password), NULL, NULL, &mountpoints);
        
        [[self mountProcessIndicator] stopAnimation:sender];
        [[self mountProcessIndicator] setHidden:YES];
        
        if (result == 0) {
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setMessageText:@"Bitte denken Sie daran, Ihr Benutzerverzeichnis nach dem Speichen wieder auszuwerfen, um Missbrauch zu verhindern."];
            [alert runModal];
            
            [self endMountSheet:sender];
            
            NSURL *folderURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", @"/Volumes", username]];
            [[NSWorkspace sharedWorkspace] openURL:folderURL];
        }
        else {
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setMessageText:@"Es ist ein Fehler aufgetreten. Das tut uns leid. :("];
            [alert runModal];
            
            NSLog(@"%@", [[NSError errorWithDomain:NSOSStatusErrorDomain code:result userInfo:nil] description]);
            NSLog(@"%@", [[NSError errorWithDomain:NSOSStatusErrorDomain code:result userInfo:nil] localizedDescription]);
        }
    }
}

- (IBAction)showUnmountSheet:(id)sender
{
    [[NSApplication sharedApplication] beginSheet:[self unmountSheet] modalForWindow:[self window] modalDelegate:self didEndSelector:nil contextInfo:nil];
    for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:@"/Volumes" error:nil]) {
        [[self unmountSelect] addItemWithTitle:file];
    }
}

- (IBAction)endUnmountSheet:(id)sender
{
    [[self unmountSelect] removeAllItems];
    [[NSApplication sharedApplication] endSheet:[self unmountSheet]];
    [[self unmountSheet] orderOut:sender];
}

- (IBAction)unmount:(id)sender
{
    NSString *path = [NSString stringWithFormat:@"/Volumes/%@", [[self unmountSelect] titleOfSelectedItem]];
    // NSLog(@"%@", path);
    int err = unmount([path cStringUsingEncoding:NSUTF8StringEncoding], 0);
    if(err > -1) {
        [self endUnmountSheet:sender];
    }
    else {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Es ist ein Fehler aufgetreten. Das tut uns leid. :("];
        [alert runModal];
    }
}

@end
