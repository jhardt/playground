//
//  main.c
//  GermanKeyboard
//
//  Created by Julius Hardt on 25.10.13.
//  Copyright (c) 2013 Julius Hardt. All rights reserved.
//

#include <CoreFoundation/CoreFoundation.h>
#include <Carbon/Carbon.h>

int main(int argc, const char * argv[])
{
    const void *keys[] = { kTISPropertyInputSourceID };
    const void *values[] = { CFSTR("com.apple.keylayout.German") };
    
    CFDictionaryRef properties = CFDictionaryCreate(kCFAllocatorDefault, keys, values, 1, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    CFArrayRef inputSourceList = TISCreateInputSourceList(properties, true);
    TISInputSourceRef germanKeyboardLayout = (TISInputSourceRef) CFArrayGetValueAtIndex(inputSourceList, 0);
    
    TISEnableInputSource(germanKeyboardLayout);
    TISSelectInputSource(germanKeyboardLayout);
    
    CFRelease(properties);
    CFRelease(inputSourceList);
    
    CFArrayRef newInputSourceList = TISCreateInputSourceList(NULL, false);
    int count = (int) CFArrayGetCount(newInputSourceList);
    for (int i = 0; i < count; i++) {
        TISInputSourceRef keyboardLayout = (TISInputSourceRef) CFArrayGetValueAtIndex(newInputSourceList, (CFIndex) i);
        if (!(kCFCompareEqualTo == CFStringCompare(TISGetInputSourceProperty(keyboardLayout, kTISPropertyInputSourceID), CFSTR("com.apple.keylayout.German"), 0))) {
            TISDisableInputSource(keyboardLayout);
        }
    }
    
    CFRelease(newInputSourceList);
    
    return 0;
}