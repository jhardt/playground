//
//  MSOAppDelegate.h
//  MSOInspireControls
//
//  Created by Julius Hardt on 15/04/14.
//  Copyright (c) 2014 Julius Hardt. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MSOAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification;
- (void)appTerminated:(NSNotification *)aNotification;
- (void)closeInspire;
- (IBAction)closeButtonPressed:(id)sender;
- (IBAction)logoutButtonPressed:(id)sender;


@end
