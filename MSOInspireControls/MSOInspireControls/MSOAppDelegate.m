//
//  MSOAppDelegate.m
//  MSOInspireControls
//
//  Created by Julius Hardt on 15/04/14.
//  Copyright (c) 2014 Julius Hardt. All rights reserved.
//

#import "MSOAppDelegate.h"
#import <Carbon/Carbon.h>

@implementation MSOAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    [self.window setLevel:kCGMainMenuWindowLevel-1];
    [self.window setCollectionBehavior:NSWindowCollectionBehaviorStationary|NSWindowCollectionBehaviorCanJoinAllSpaces|NSWindowCollectionBehaviorFullScreenAuxiliary];
    
    NSPoint position;
    position.x = [[NSScreen mainScreen] visibleFrame].origin.x;
    position.y = [[NSScreen mainScreen] visibleFrame].origin.y;
    [self.window setFrameOrigin:position];
    
    NSNotificationCenter *center = [[NSWorkspace sharedWorkspace] notificationCenter];
    [center addObserver:self selector:@selector(appTerminated:) name:NSWorkspaceDidTerminateApplicationNotification object:nil];
    
}

- (void)appTerminated:(NSNotification *)aNotification
{
    if ([[[aNotification userInfo] objectForKey:@"NSApplicationBundleIdentifier"] isEqualToString:@"com.prometheanworld.ActivSoftware.ActivInspire.1"]) {
        [[NSApplication sharedApplication] terminate:self];
    }
}

- (void)closeInspire
{
    for(NSRunningApplication *application in [NSRunningApplication runningApplicationsWithBundleIdentifier:@"com.prometheanworld.ActivSoftware.ActivInspire.1"])
    {
        [application forceTerminate];
    }
}

- (IBAction)closeButtonPressed:(id)sender {
    [self closeInspire];
    [[NSApplication sharedApplication] terminate:self];
}

- (IBAction)logoutButtonPressed:(id)sender {
    [self closeInspire];
    SendAppleEventToSystemProcess(kAEReallyLogOut);
}

OSStatus SendAppleEventToSystemProcess(AEEventID EventToSend)
{
    AEAddressDesc targetDesc;
    static const ProcessSerialNumber kPSNOfSystemProcess = { 0, kSystemProcess };
    AppleEvent eventReply = {typeNull, NULL};
    AppleEvent appleEventToSend = {typeNull, NULL};
    
    OSStatus error = noErr;
    
    error = AECreateDesc(typeProcessSerialNumber, &kPSNOfSystemProcess,
                         sizeof(kPSNOfSystemProcess), &targetDesc);
    
    if (error != noErr)
    {
        return(error);
    }
    
    error = AECreateAppleEvent(kCoreEventClass, EventToSend, &targetDesc,
                               kAutoGenerateReturnID, kAnyTransactionID, &appleEventToSend);
    
    AEDisposeDesc(&targetDesc);
    if (error != noErr)
    {
        return(error);
    }
    
    error = AESend(&appleEventToSend, &eventReply, kAENoReply,
                   kAENormalPriority, kAEDefaultTimeout, NULL, NULL);
    
    AEDisposeDesc(&appleEventToSend);
    if (error != noErr)
    {
        return(error);
    }
    
    AEDisposeDesc(&eventReply);
    
    return(error); 
}

@end
