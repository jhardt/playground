// Playground - noun: a place where people can play

// import UIKit
import Foundation

enum PersonalPronoun {
    case FirstSingular, SecondSingular, ThirdSingular, FirstPlural, SecondPlural, ThirdPlural
}

let word = "to play" as NSString
let verb = word.substringFromIndex(3)
let form = PersonalPronoun.ThirdSingular

if form == .ThirdSingular {
    println(verb + "s")
} else {
    println(verb)
}