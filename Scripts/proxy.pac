function FindProxyForURL (url, host) {
	if (dnsDomainIs(host, ".marienschule.com") || isPlainHostName(url) || host.indexOf(".local") > -1 || isInNet(host, "10.1.0.0", "255.255.0.0")) {
		return "DIRECT";
	}
	else {
		return "PROXY routing:8080; DIRECT";
	}
}