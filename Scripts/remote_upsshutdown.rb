#!/usr/bin/env ruby

# /usr/libexec/upsshutdown

# | https://github.com/capistrano/sshkit
# | https://github.com/mikel/mail
# $ gem install sshkit mail

require 'sshkit/dsl'
require 'mail'

all_servers = %w{turing.marienschule.com neumann.marienschule.com}
message = 'Shutting down due to power loss!'


on all_servers, in: :parallel do |host|
	as :admin do
		execute :logger, '-i', '-p daemon.emerg', '-t UPS', message
		execute :shutdown, '-h now', message
	end
end


Mail.deliver do
  from     'spam@marienschule.com'
  to       ['kuehnert@marienschule.com', 'dohrn@marienschule.com']
  subject  'Stromausfall in der MSO'
  body     'Es hat einen Stromausfall in der MSO gegeben, die Server sind jedoch sicher heruntergefahren.'
  delivery_method :smtp, address: 'intern.marienschule.com', port: 1025
end