$(function() {
	$('.lesson').on('mouseover', function() {
		var course = $(this).data('course-id');
		$("[data-course-id='" + course + "']").addClass('timetable-active');
	});

	$('.lesson').on('mouseleave', function() {
		var course = $(this).data('course-id');
		$("[data-course-id='" + course + "']").removeClass('timetable-active');
	});
});