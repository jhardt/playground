function generate_anchor(content) {
	return content.replace(/\s/g, '-').replace(/[^\-\w]/g, '');
}

function normalize_level(level) {
	if (level == 4) {
		return --level;
	}	else {
		return level;
	}
}

var list = '<ul>';

var lastLevel = 0;

$('h1,h2,h3,h4,h5,h6').each(function(index, element) {
	var level = normalize_level(parseInt($(this).prop('tagName').replace(/\D/, '')));
	var heading = $(this).text();
	var anchor = generate_anchor(heading);
	$(this).attr('id', anchor);

	if (lastLevel == 0) {
		// do nothing, first element
	} else if (level == lastLevel) {
		list += '</li>';
	} else if (level > lastLevel) {
		list += '<ul>';
	} else if (level < lastLevel) {
		while (lastLevel > level) {
			list += '</li>';
			list += '</ul>';
			--lastLevel;
		}
		list += '</li>';
	}

	list += '<li>';
	list += '<a href="' + '#' + anchor + '">';
	list += heading;
	list += '</a>';

	lastLevel = level;

	console.log(level);
	console.log(heading);
});

while (lastLevel > 1) {
	list += '</li>';
	list += '</ul>';
	--lastLevel;
}
list += '</li>';

list += '</ul>';

$('<div id="tableofcontents"></div>').insertAfter('hr:first');
$('#tableofcontents').html(list);


$('a').click(function(){
  $('html, body').animate({
      scrollTop: $( $.attr(this, 'href') ).offset().top - 60
  }, 500);
  return false;
});
