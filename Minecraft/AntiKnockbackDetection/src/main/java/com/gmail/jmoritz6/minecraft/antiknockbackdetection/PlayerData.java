package com.gmail.jmoritz6.minecraft.antiknockbackdetection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class PlayerData {
    private int trustLevel = 0;
    private long velocityPackageSendTime = 0;

    public int getTrustLevel() {
        return trustLevel;
    }

    public void increaseTrustLevel() {
        // trustLevel++;
        Bukkit.broadcastMessage(ChatColor.GREEN + "New trust level: " + ++trustLevel);
    }

    public void decreaseTrustLevel() {
        // trustLevel--;
        Bukkit.broadcastMessage(ChatColor.RED + "New trust level: " + --trustLevel);
    }

    public long getVelocityPackageSendTime() {
        return velocityPackageSendTime;
    }

    public void setVelocityPackageSendTime(long velocityPackageSendTime) {
        this.velocityPackageSendTime = velocityPackageSendTime;
    }
}
