package com.gmail.jmoritz6.minecraft.antiknockbackdetection;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Map;
import java.util.WeakHashMap;

public class AntiKnockbackDetection extends JavaPlugin {
    private Map<Player, PlayerData> playerDataMap = new WeakHashMap<Player, PlayerData>();

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(new AntiKnockbackListener(this), this);
        getLogger().info("AntiKnockbackDetection has been successfully enabled.");
    }

    @Override
    public void onDisable() {
        getLogger().info("AntiKnockbackDetection has been disabled.");
    }

    public PlayerData getPlayerData(Player player) {
        PlayerData data = playerDataMap.get(player);
        if (data == null) {
            data = new PlayerData();
            playerDataMap.put(player, data);
        }
        return data;
    }
}
