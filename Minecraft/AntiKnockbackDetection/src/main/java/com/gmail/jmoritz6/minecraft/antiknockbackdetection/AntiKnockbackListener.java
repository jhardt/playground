package com.gmail.jmoritz6.minecraft.antiknockbackdetection;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

public class AntiKnockbackListener implements Listener {
    private AntiKnockbackDetection plugin;

    public AntiKnockbackListener(AntiKnockbackDetection plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player && event.getInventory().getType() == InventoryType.MERCHANT) {
            event.getWhoClicked().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, -5));

            Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
                public void run() {
                    Player player = (Player) event.getWhoClicked();

                    plugin.getPlayerData(player).setVelocityPackageSendTime(System.currentTimeMillis());
                    player.setVelocity(new Vector(0, 0.07F, 0));
                }
            }, 1);
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        PlayerData data = plugin.getPlayerData(player);

        if (data.getVelocityPackageSendTime() != 0) {
            if (event.getFrom().getY() < event.getTo().getY()) {
                data.increaseTrustLevel();
            }
            else {
                data.decreaseTrustLevel();
            }

            data.setVelocityPackageSendTime(0);
            player.removePotionEffect(PotionEffectType.JUMP);
        }
    }
}
